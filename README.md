# OSOBINE SISTEMA PREKIDANJA 

Sa tehničke tačke gledišta neophodno je da se napred navedene funkcije sistema prekidanja mogu izvršavati dovoljno brozo sa mninimalnim gubitkom vremena i sa maksimalnom fleksibilnošču. Za tehničku ocjenu sistema prekidanja koriste se sledeći parametri.

## 1. VRIJEME REAKCIJE

Kao što smo to već napomenuli vrijeme reakcije predstavlja vremenski interval koji protekne od pojave signala prekida pa do započinjanja izvršavanja prve instrukcije odgovarajućeg prekidnog programa. S obzirom da kod računarskog sistema može da postoji više izvora prekida koji se opslužuju sa različitim prioritetom, to u savisnosti od toga koliko se pojavilo signala prekida većih prioriteta i vrijeme opsluživanja signala prioriteta može da varira. Zbog toga se kao parametar za ocjenu sistema prekidanja koristi vrijeme reakcije u odnosu na signal prekida  sa najvećim prioritetom.

## 2. VRIJEME OPSLUŽIVANJA PREKIDA 

Vrijeme opsluživanja prekida predstavlja vrijeme između punog vremena opsluživanja prekida i vrijeme izvršavanja korisnih instrukcija sopstvenog prekidnog programa.

## PROMJENA
